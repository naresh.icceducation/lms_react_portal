import React from 'react';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { useHistory } from 'react-router-dom';
import { Button } from 'reactstrap';

const HistoryBack = () => {
  const history = useHistory();
  return (
    <>
      <Button color="link" onClick={history.goBack}>
        <ArrowBackIcon />
      </Button>
    </>
  );
};

export default HistoryBack;
