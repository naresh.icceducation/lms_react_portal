import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { Button, TextField } from '@material-ui/core';
// import * as yup from 'yup';
import { useHistory } from 'react-router-dom';
// import { yupResolver } from '@hookform/resolvers/yup';
import BlankNav from '../../containers/navs/BlankNav';
import '../../assets/css/sass/views/register.scss';

const CompanyDet = () => {
  const [companyDet, setCompanyDet] = useState();
  const [jobTitle, setjobTitle] = useState();
  const history = useHistory();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: 'onBlur',
    reValidateMode: 'onChange',
  });

  return (
    <div className="bg">
      <BlankNav />
      <div
        className="centerCard mx-auto"
        style={{
          width: '25%',
        }}
      >
        <h2 className="text-center">Try CMS Premium free for 30day</h2>
        <span className="Topbox">Step 1 of 3</span>
        <div className="card pt-5 pb-5 pl-5 pr-5">
          <h6 className="font-weight-bold text-center pb-4">
            First, tell us a bit about yourself and your company
          </h6>

          <form
            onSubmit={handleSubmit((data) => {
              sessionStorage.setItem(' companyDet', JSON.stringify(data));
              console.log(data);
              history.push('/accountSetup');
            })}
          >
            <div className="mb-3">
              <TextField
                fullWidth
                id="companyDet"
                label="Your company name"
                name="companyDet"
                variant="outlined"
                onChange={(e) => setCompanyDet(e.target.value)}
                value={companyDet}
                {...register('companyDet', {
                  required: 'company Details is required',
                  minLength: {
                    value: 5,
                    message: 'Minimum 5 Characters Required for User Name',
                  },
                })}
              />
              {errors.companyDet && (
                <p className="error"> {errors.companyDet.message}</p>
              )}
            </div>
            <div className="mb-3">
              <TextField
                type="text"
                fullWidth
                name="jobTitle"
                label="Your job title (optional)"
                variant="outlined"
                value={jobTitle}
                onChange={(e) => setjobTitle(e.target.value)}
                id="jobTitle"
                {...register('jobTitle', {
                  minLength: {
                    value: 5,
                    message: 'Minimum 5 Characters Required for jobTitle',
                  },
                })}
              />
              {errors.jobTitle && (
                <p className="error"> {errors.jobTitle.message}</p>
              )}
            </div>

            <Button
              className="btn_next"
              color="primary"
              variant="contained"
              type="submit"
            >
              Next
            </Button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default CompanyDet;
